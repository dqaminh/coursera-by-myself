var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}
var gBASE_URL = "https://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses";

$(document).ready(function() {
    onPageLoading();
})

//Get courses list
function onPageLoading() {
    $.ajax({
        url: gBASE_URL,
        method: 'GET',
        dataType: 'json',
        success: function(response) {
            console.log(response);
            loadDataIntoCards(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log('Error:', textStatus, errorThrown);
        }
    });
}

//load data into cards
function loadDataIntoCards(paramresponse) {

    var courses = paramresponse;
    var trendingcards = $(".trending");
    var popularcards = $(".popular")

    loadTrendingCourses(courses,trendingcards);
    loadPopularCourses(courses,popularcards);
}

//load popular courses
function loadPopularCourses(courses,popularcards) {

     //select random 4 popular courses
     var popularCourses = courses.filter(course => course.isTrending);
     var selectedCourses = [];
     while (selectedCourses.length < 4 && popularCourses.length > 0) {
         const randomIndex = Math.floor(Math.random() * popularCourses.length);
         selectedCourses.push(popularCourses.splice(randomIndex, 1)[0]);
         }
     console.log(selectedCourses);
     
     //get 4 courses to load into cards
     popularcards.each((index,card) => {
         var course = selectedCourses[index];
         var $img = $(card).find('.card-top img');
         var $name = $(card).find('.card-body h4');
         var $duration = $(card).find('.card-body small:nth-of-type(1)');
         var $level = $(card).find('.card-body small:nth-of-type(2)');
         var $salePrice = $(card).find('.card-body h5');
         var $price = $(card).find('.card-body del');
         var $avatar = $(card).find('.card-footer img');
         var $teacher = $(card).find('.card-footer small');
 
         $img.attr('src', course.coverImage);
         $name.text(course.courseName);
         $duration.text(course.duration);
         $level.text(course.level);
         $salePrice.text(`$${course.discountPrice}`);
         $price.text(`$${course.price}`);
         $avatar.attr('src', course.teacherPhoto);
         $teacher.text(course.teacherName);
     })
}

//load trending courses
function loadTrendingCourses(courses,trendingcards) {

     //select random 4 trending courses
     var trendingCourses = courses.filter(course => course.isTrending);
     var selectedCourses = [];
     while (selectedCourses.length < 4 && trendingCourses.length > 0) {
         const randomIndex = Math.floor(Math.random() * trendingCourses.length);
         selectedCourses.push(trendingCourses.splice(randomIndex, 1)[0]);
         }
     console.log(selectedCourses);
     
     //get 4 courses to load into cards
     trendingcards.each((index,card) => {
         var course = selectedCourses[index];
         var $img = $(card).find('.card-top img');
         var $name = $(card).find('.card-body h4');
         var $duration = $(card).find('.card-body small:nth-of-type(1)');
         var $level = $(card).find('.card-body small:nth-of-type(2)');
         var $salePrice = $(card).find('.card-body h5');
         var $price = $(card).find('.card-body del');
         var $avatar = $(card).find('.card-footer img');
         var $teacher = $(card).find('.card-footer small');
 
         $img.attr('src', course.coverImage);
         $name.text(course.courseName);
         $duration.text(course.duration);
         $level.text(course.level);
         $salePrice.text(`$${course.discountPrice}`);
         $price.text(`$${course.price}`);
         $avatar.attr('src', course.teacherPhoto);
         $teacher.text(course.teacherName);
     })
}